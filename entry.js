var express = require('express');
var expressValidator = require('express-validator');
var bodyParser = require('body-parser');
var path = require('path');


var url = require('url');

var mongojs = require('mongojs');
var db_cw = mongojs('weatherapp', ['current_weather']);
var db_w = mongojs('weatherapp', ['weather']);
var ObjectId = mongojs.ObjectId;

//Forecase IO 
var ForecastIo = require('forecastio');
var forecastIo = new ForecastIo('4de99a920303a2d56853701fd23d2fa3');
var forecastOptions = {
    exclude: 'minutely,hourly,flags',
};

// Google API
var NodeGeocoder = require('node-geocoder');
var geoOptions = {
  provider: 'google',

//   Optional depending on the providers 
httpAdapter: 'https', // Default 
    // apiKey: process.env.GOOGLE_API_KEY, // for Mapquest, OpenCage, Google Premier 
  apiKey: 'AIzaSyBeVXowDcTxiMuKMyq5u1lOLIRHU673TWc', 
  formatter: null         // 'gpx', 'string', ... 
};

var geocoder = NodeGeocoder(geoOptions);

var moment = require('moment');

var Highcharts = require('highcharts');

var app = express();

//View Engine
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));

//Body Parser Middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}))

//Set Static Path
app.use(express.static(path.join(__dirname, 'public')))

//Global Vars
app.use(function(req, res, next){
    res.locals.errors = null;
    next();
});

// Express Validator Middleware
app.use(expressValidator({
  errorFormatter: function(param, msg, value) {
      var namespace = param.split('.')
      , root    = namespace.shift()
      , formParam = root;

    while(namespace.length) {
      formParam += '[' + namespace.shift() + ']';
    }
    return {
      param : formParam,
      msg   : msg,
      value : value
    };
  }
}));

var chart_categories = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
var chart_options = [1, 2, 3, 4, 6, 7, 9];

var g_option = [
          ['Mushrooms', 3],
          ['Onions', 1],
          ['Olives', 1],
          ['Zucchini', 1],
        ];

app.get('/', function(req, res) {     
    var fc;
    var loc = loc = req.query.loc;
    var cur_temp, cur_summary, day_summary;
    
    if(req.query.fc != undefined) {
        fc = JSON.parse(req.query.fc);
    }

    db_w.weather.find(function(err, result) {
        // console.log(rs);
        res.render('index', {
            cur_loc: loc,
            searhed_history: result,
            moment: moment,
            fc: fc,
        });
    })

});


app.post('/weather/details', function(req, res) {
    req.checkBody('location', 'Please Enter location').notEmpty();
    var errors = req.validationErrors();

    if(errors){

    } else {
        // console.log('Display Forecast Clicked');
        var loc = req.body.location;
        var dt = req.body.date;

        // console.log('Google Geocoder Request');
        geocoder.geocode(loc, function(err, result) {
            // console.log(result);
            var long = result[0].longitude;
            var lat = result[0].latitude;
            var address = result[0].formattedAddress;


            if(dt != '') {
                var time = new Date(dt).toISOString().substring(0,19) + 'Z';
                forecastIo.timeMachine(lat, long, time, forecastOptions).then(function(data) {
                    // console.log(JSON.stringify(data, null, 2));
                    dbinsert(address,data, res);
                });
            } else {
                forecastIo.forecast(lat, long, forecastOptions).then(function(data) {
                    // console.log(JSON.stringify(data, null, 2));
                    dbinsert(address,data, res);
                });
            }
        });
    };
    
});

function dbinsert(address, fcresult, response) {

    var newWeather = {
        'address': address,
        'temperature': fcresult.currently.temperature,
    }

    db_w.weather.insert(newWeather, function(err, result){
        if(err){
            console.log(err);
        } else {
            response.redirect(url.format({
                pathname: '/',
                query: {
                    "loc": address,
                    "fc": JSON.stringify(fcresult, null, 2)
                }
            })); 
        }
    });
}



app.delete('/history/clear', function(req, res){
    db_w.weather.remove({function(err, result) {
        if(err){
            console.log(err);
        }     
        res.redirect('/');
    }});
});

app.listen(3000, function(){
    console.log('Server Started on Port 80...');
})