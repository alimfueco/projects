$(document).ready(function(){
    $('.clearHistory').on('click', clearHistory);
});

function submitForm() {
    $('#searchForm').submit()
}


function clearHistory(){
    var confirmation = confirm("Are you sure?");

    if(confirmation) {
        $.ajax({
            type: 'DELETE',
            url: '/history/clear/'
        }).done(function(response){
            window.location.replace('/');
        });
         window.location.replace('/');
    } else {
        return false;
    }
}

var chart_categories = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
var chart_options = [1, 2, 3, 4, null, 6, 7, null, 9];

$(function () { 
    var myChart = Highcharts.chart('chart-container', {
        title: {
            text: 'Time Machine Temperature'
        },
        xAxis: {
            categories: chart_categories
        },
        series: [{
            data: chart_options,
            step: 'right',
            name: 'Date'
        }]
    });
});